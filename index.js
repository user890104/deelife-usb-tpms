import { argv, argv0, exit } from 'node:process';
import { createSerialPort, writeToPort } from './port.js';
import { createParser } from './parser.js';
import { createDecoder } from './decoder.js';
import { querySensorId } from './encoder.js';

if (argv.length !== 3) {
    console.error('Usage: ' + argv0 + ' ' + argv[1] + ' </dev/ttyXXX>');
    exit(1);
}

const description = '1';
const port1 = createSerialPort(argv[2], description);
const parser = createParser();
port1.pipe(parser);

const decoder = createDecoder();
parser.on('data', decoder.handler);

Object.keys(decoder.events).forEach(eventKey => {
    const eventSymbol = decoder.events[eventKey];

    if (eventSymbol.description === 'Tyre status') {
        return;
    }

    decoder.emitter.addListener(eventSymbol, (...args) => {
        console.log(eventSymbol.description, args);
    });
});

port1.on('open', async () => {
    await writeToPort(port1, querySensorId(), description);
});
