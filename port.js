import { SerialPort } from 'serialport';

const hexdump = data => Array.from(data).map(num =>
    num.toString(16).padStart(2, '0')
).join(' ');

const logger = (info, data) => {
    console.log('[' + (new Date).toISOString() + '] ' + info + ': ' + hexdump(data));
};

export const createSerialPort = (path, description, onData) => {
    const serialPort = new SerialPort({
        path: path,
        baudRate: 19200,
    });

    serialPort.on('data', data => {
        if (description) {
            logger('Port ' + description + ' receive', data);
        }

        onData && onData(data);
    });

    return serialPort;
};

export const showPortInfo = (ports, path, description) => {
    const port = ports.filter(port => port.path === path).shift();

    if (!port) {
        console.error('Port ' + description + '(' + path + ') not found');
    }

    console.log('Port ' + description + ': ' + port.path + (port.vendorId && port.productId ?
            (' [' + port.vendorId + ':' + port.productId + ']') : ''
    ));
};

export const writeToPort = (port, data, description) => new Promise((resolve, reject) => {
    if (description) {
        logger('Port ' + description + ' send', data);
    }

    port.write(data, err => err ? reject(err) : resolve());
});
