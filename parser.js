import { Transform } from 'node:stream';
import { prefix, xorChecksum } from './protocol.js';

let buffer = Buffer.alloc(0);

export const createParser = () => {
    const transform = new Transform({
        transform: (chunk, encoding, done) => {
            buffer = Buffer.concat([buffer, chunk]);

            let pos = 0;
            let len;

            while (
                ((pos = buffer.indexOf(prefix, pos)) !== -1) &&
                buffer.length >= pos + 3 &&
                buffer.length >= pos + (len = buffer.readUint8(pos + 2)) &&
                xorChecksum(Array.from(buffer.slice(pos, pos + len))) === 0
            ) {
                if (pos > 0) {
                    console.warn('Transform leftover head: ', buffer.slice(0, pos));
                }

                transform.push(buffer.slice(pos + 3, pos + len - 1));
                buffer = buffer.slice(pos + len);
            }

            done();
        },
    });

    return transform;
};
