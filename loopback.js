import { argv, argv0, exit } from 'node:process';
import { SerialPort } from 'serialport';
import { createSerialPort, showPortInfo } from './port.js';

if (argv.length !== 4) {
    console.error('Usage: ' + argv0 + ' ' + argv[1] + ' </dev/ttyXXX> </dev/ttyZZZ>');
    exit(1);
}

const port1Path = argv[2];
const port2Path = argv[3];

const ports = await SerialPort.list();

showPortInfo(ports, port1Path, '1');
showPortInfo(ports, port2Path, '2');

let port2 = {
    write: () => {},
};

const port1 = createSerialPort(port1Path, '1', data => port2.write(data));
port2 = createSerialPort(port2Path, '2', data => port1.write(data));
