export const getKey = (obj, value) => Object.entries(obj).filter(([_, v]) =>
    v === value,
).shift()?.[0];
