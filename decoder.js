import { EventEmitter } from 'node:events';
import { positions, positionsFind } from './protocol.js';

const events = {
    HEARTBEAT: Symbol('Heartbeat'),
    PAIRING_SUCCESSFUL: Symbol('Pairing successful'),
    TIME_SEED: Symbol('Time seed'),
    SWAP_TYRES: Symbol('Swap tyres'),
    TYRE_STATUS: Symbol('Tyre status'),
    SENSOR_ID: Symbol('Sensor ID'),
    UNHANDLED_PACKET: Symbol('Unhandled packet'),
};

export const createDecoder = () => {
    const emitter = new EventEmitter();
    const handler = data => {
        switch (data.length) {
            case 2:
                switch (data[0]) {
                    case 0x00:
                        if (data[1] === 0x88) {
                            emitter.emit(events.HEARTBEAT);
                        }

                        break;
                    case 0x16:
                        // no action
                        break;
                    case 0x18:
                        emitter.emit(events.PAIRING_SUCCESSFUL, positions[data[1]]);
                        break;
                    case 0xB5:
                        emitter.emit(events.TIME_SEED, data[1]);
                        break;
                }
                break;
            case 3:
                if (data[0] === 0x30) {
                    emitter.emit(events.SWAP_TYRES, positions[data[1]], positions[data[2]]);
                }
                break;
            case 4:
                emitter.emit(events.TYRE_STATUS, positions[data[0]], {
                    airPressure: data[1] * 3.44,
                    temperature: data[2] - 50,
                    leakage: (data[3] & (1 << 3)) !== 0,
                    lowPower: (data[3] & (1 << 4)) !== 0,
                    noSignal: (data[3] & (1 << 5)) !== 0,
                });
                break;
            case 5:
                const ID = Buffer.from(data.slice(1, 5)).readUInt32LE();
                emitter.emit(events.SENSOR_ID, positionsFind[data[0]], ID);
                break;
            default:
                emitter.emit(events.UNHANDLED_PACKET, data);
        }
    };

    return {
        events,
        handler,
        emitter,
    };
};
