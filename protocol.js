import { getKey } from './utils.js';

export const prefix = Uint8Array.from([0x55, 0xAA]);

export const positions = {
    0x00: 'FL',
    0x01: 'FR',
    0x10: 'RL',
    0x11: 'RR',

    0x05: 'S',
};

export const positionsFind = {
    1: 'FL',
    2: 'FR',
    3: 'RL',
    4: 'RR',

    5: 'S',
};

export const xorChecksum = arr => arr.reduce((prev, curr) => prev ^ curr, 0);
export const positionNameToCode = pos => getKey(positions, pos);
