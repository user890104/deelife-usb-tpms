import { positionNameToCode, prefix, xorChecksum } from './protocol.js';

export const encode = data => {
    const packet = Array.from(prefix).concat([data.length + 4]).concat(data);
    return packet.concat(xorChecksum(packet));
};

export const heartbeatEventAck = () => encode([0x00, 0x00]);
export const pair = position => encode([0x01, positionNameToCode(position)]);
export const exchange = (position1, position2) => encode([
    0x03,
    positionNameToCode(position1),
    positionNameToCode(position2),
]);
export const stopPairing = () => encode([0x06, 0x00]);
export const querySensorId = () => encode([0x07, 0x00]);
export const sendHeartbeat = () => encode([0x19, 0x00]);
export const sendEncryption = seed => encode([0x5b, seed]);
export const resetDevice = () => encode([0x58, 0x55]);
